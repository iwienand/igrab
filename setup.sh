#!/bin/bash

# By Derek Barbosa <debarbos@redhat.com>
# For use on Fedora Systems (maybe RHEL too?)
# Automate setup of python packages and aliasing :)
# Making plenty of general assumptions here:
# You have Python 3.10 or greater installed
# Your local bin is in your PATH
# System is resistant to stray solar rays/flares/storms

minVersion=10

pyVerNum=$(python3 -c 'import sys; print (sys.version_info)' | grep -o 'minor=[0-9][0-9]' | cut -d = -f 2)

if ((pyVerNum >= minVersion)) 
then
  echo Version Number is: ${pyVerNum}
  echo Success, Python version is of 3.10 or higher 
else
  echo Please update your system Python Version to Version 3.10 or higher
  exit 1
fi

python3.${pyVerNum} -m pip install -r requirements.txt

mkdir -p ~/.local/bin/

cp ./src/igrab.py ~/.local/bin/igrab

chmod ugo+x ~/.local/bin/igrab

sed -i '1i#!/usr/bin/python' ~/.local/bin/igrab

sleep 1

echo
echo "If \$HOME/local/bin is not appended to your \$PATH, please do so via the following command "
echo "export \$PATH:\$HOME/.local/bin"
echo
echo "Note that this is a temporary change, to permanently add ~/.local/bin to your PATH..."
echo "Please edit your bash/zsh/fish/sh/etc profile or .rc file"
echo "e.g. \$PATH:\$HOME/.local/bin"
echo
