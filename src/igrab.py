from columnar import columnar
from datetime import datetime
import sys
import click
import koji
import requests
import base64
import gzip
import xmltodict

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])

#########################
# CLASSES AND DATATYPES #
#########################

# Definitions of specific datatypes.


# store cli args here
class cliData:
    def __init__(self, id, nvr, testcase):
        self.id = id
        self.nvr = nvr
        self.testcase = testcase


# igrab response type, stores OSCI data
class osciData:
    def __init__(self, satisfied, unsatisfied, waived, umbRefs):
        # satisfied and unsatisfied are dictionaries
        self.satisfied = satisfied
        self.unsatisfied = unsatisfied

        # stores the corresponding UMB messages for testcases
        self.umbRef = umbRefs

        # waived is a list
        self.waived = waived


# datatype to store testcase items
class testData:
    def __init__(self, testcaseData, umbURL):
        self.testcaseData = testcaseData
        self.umbURL = umbURL


#########################
#   HELPER FUNCTIONS    #
#########################

# These functions are responsible for input "checking" and lifting the brunt of
# the code logic within the main function loop
# these are usually labelled nounHandler()


# Handle the logic regarding sending and returning requests with an NVR
def nvrHandler(nvr):
    click.secho("\n  Entered NVR: " + nvr + "\n", fg="bright_yellow")
    try:
        brewResponse = brewNVRSearch(nvr)
        click.secho(
            f"  Brew Data: https://brewweb.engineering.redhat.com/brew/buildinfo?buildID={brewResponse['buildID']}",
            fg="bright_green",
        )
        brewTable(brewResponse)
    except:
        click.secho("  Brew Timeout Error \n", fg="red", bold=True)
        sys.exit(
            click.echo(
                "  Please ensure https://brewweb.engineering.redhat.com/ is not down."
            )
        )
    return osciSearch(nvr, brewResponse["Volume"])


# Same as the nvrHandler but with Task IDs
def idHandler(id):
    click.secho("\n  Entered Task ID: " + str(id) + "\n", fg="bright_yellow")
    try:
        brewResponse = brewIDSearch(id)
        nvr = brewResponse["nvr"]
        click.secho("  Retrieved NVR: " + str(nvr), fg="bright_yellow")
        click.secho(
            f"  Brew Data: https://brewweb.engineering.redhat.com/brew/buildinfo?buildID={brewResponse['buildID']}",
            fg="bright_green",
        )
        brewTable(brewResponse)
    except:
        click.secho("  Brew Timeout Error \n", fg="red", bold=True)
        sys.exit(
            click.echo(
                "  Please ensure https://brewweb.engineering.redhat.com/ is not down."
            )
        )
    return osciSearch(nvr, brewResponse["Volume"])


# Handle the logic of requesting information on a testcase
def testcaseHandler(cliData, osciData, testName):
    click.secho("\n  Input: " + str(testName), fg="bright_yellow")
    try:
        if cliData.nvr is None and cliData.id is None:
            sys.exit(
                click.style(
                    "  Please supply either an NVR or Task ID FIRST \n",
                    fg="bright_red",
                    bold=True,
                )
            )
        return testcaseSearch(osciData, testName)
    except:
        click.secho("  ResultsDB Timeout Error \n", fg="red", bold=True)
        sys.exit(
            click.echo(
                """  Please Ensure that resultsdb-api.engineering.redhat.com is
                       reachable."""
            )
        )


#########################
# API HANDLER FUNCTIONS #
#########################

# These functions are responsible for sending queries to respective API
# endpoints, unmarshalling data, and creating the proper data structures.
# these are usually labeled endpointSearch()


# Call Brew API and return formatted dictionary
# (as opposed to a list of dicts)
def brewNVRSearch(nvr):
    session = koji.ClientSession("https://brewhub.engineering.redhat.com/brewhub")
    response = session.getBuild(str(nvr))
    if response is None:
        sys.exit(
            click.style(
                """  No Brew Data is available for this build
            \n  Either you have entered an incorrect ID/NVR or there is no build data available
            \n  Please try running the tool again, if the issue persists, please visit the Brew web UI
            \n  Or contact debarbos
            """,
                fg="red",
                bold=True,
            )
        )
    titles = [
        "Owner",
        "nvr",
        "buildID",
        "taskID",
        "Source",
        "Volume",
        "Started",
        "Finished",
    ]
    results = [
        response["owner_name"],
        response["nvr"],
        response["build_id"],
        response["task_id"],
        response["source"],
        response["volume_name"],
        response["start_time"],
        response["completion_time"],
    ]
    return dict(zip(titles, results))


# Same as NVR search, but with Task IDs
def brewIDSearch(id):
    session = koji.ClientSession("https://brewhub.engineering.redhat.com/brewhub")
    response = session.listBuilds(taskID=int(id))
    if response is None:
        sys.exit(
            click.style(
                """  No Brew Data is available for this build
            \n  Either you have entered an incorrect ID/NVR or there is no build data available
            \n  Please try running the tool again, if the issue persists, please visit the Brew web UI
            \n  Or contact debarbos
            """,
                fg="red",
                bold=True,
            )
        )
    titles = [
        "Owner",
        "nvr",
        "buildID",
        "taskID",
        "Source",
        "Volume",
        "Started",
        "Finished",
    ]
    results = [
        response[0]["owner_name"],
        response[0]["nvr"],
        response[0]["build_id"],
        response[0]["task_id"],
        response[0]["source"],
        response[0]["volume_name"],
        response[0]["start_time"],
        response[0]["completion_time"],
    ]
    return dict(zip(titles, results))


# Queries endpoint, creates osci object
def osciSearch(nvr, product):
    dashboard_url = (
        "https://dashboard.osci.redhat.com/#/artifact/brew-build/nvr/" + str(nvr)
    )
    click.secho("  OSCI Data: " + dashboard_url, fg="bright_green")

    graphql_url = "https://dashboard.osci.redhat.com/graphql"
    payload = """
    query decision {
        greenwave_decision (product_version: "%s",
        subject: [{item:"%s", type:"brew-build"}],
        decision_context: "osci_compose_gate") {
        unsatisfied_requirements
        satisfied_requirements
        results
      }
    waiver_db_waivers(subject_identifier: "%s") {
        waivers {
          testcase
        }
      }
    }
    """ % (
        product,
        nvr,
        nvr,
    )
    query = {"query": payload}
    ret = requests.post(graphql_url, json=query)

    if ret.ok:
        waivedTests = []
        unsatisfiedReqs, umbReferences, satisfiedReqs = {}, {}, {}

        jsonData = ret.json().get("data")
        greenwaveDecision = jsonData.get("greenwave_decision")

        if greenwaveDecision:
            unsatisfiedRequirementResponse = greenwaveDecision.get("unsatisfied_requirements")

            satisfiedRequirementResponse = greenwaveDecision.get("satisfied_requirements")

            waivedResponse = jsonData.get("waiver_db_waivers").get("waivers")

            waivedTests += (item["testcase"] for item in waivedResponse)

            # Since HREF field DNE in un/satisfied_requirements, query for HREF data
            resultsResponse = greenwaveDecision.get("results")

        if greenwaveDecision == None or len(resultsResponse) == 0:
            sys.exit(
                click.style(
                    """\n  No OSCI Data is available for this NVR/ID
              \n  Either you have entered an incorrect NVR/Id or there is no testcase data available yet
              \n  Please try running the tool again, if the issue persists, please visit the OSCI web UI
              \n  Or contact debarbos
              """,
                    fg="red",
                    bold=True,
                )
            )

        satisfied_test_names = set(
            satisfied_test["testcase"]
            for satisfied_test in satisfiedRequirementResponse
        )
        unsatisfied_test_names = set(
            unsatisfied_test["testcase"]
            for unsatisfied_test in unsatisfiedRequirementResponse
        )
        for item in resultsResponse:
            testcase_name = item["testcase"]["name"]

            if testcase_name in satisfied_test_names:
                satisfiedReqs[testcase_name] = item["href"]
                umbReferences[testcase_name] = item["data"]["msg_id"]

            if testcase_name in unsatisfied_test_names:
                unsatisfiedReqs[testcase_name] = item["href"]
                umbReferences[testcase_name] = item["data"]["msg_id"]

        return osciData(satisfiedReqs, unsatisfiedReqs, waivedTests, umbReferences)

    return osciData(None, None, None, None)


# Searches for specific testcase resultsDB data using the osciData object
# Supports partial matches
def testcaseSearch(osciData, testName):
    osciDict = osciData.satisfied | osciData.unsatisfied
    matchedTests = [i for i in osciDict if testName in i]
    matchedUMB = osciData.umbRef[matchedTests[0]][0]
    umbURL = f"https://datagrepper.engineering.redhat.com/id?id={matchedUMB}&is_raw=true&size=extra-large"

    if len(matchedTests) == 0:
        sys.exit(
            click.style(
                """  No gating testcases match input
            \n  Either the specified testcase does not exist, or your search term is incorrect
            \n  Please try again
            """,
                fg="red",
                bold=True,
            )
        )

    # Use the resultsdb url for the first match in the merged testcase dictionary
    dbURL = osciDict[matchedTests[0]]
    click.secho(f"  Matched Testsuite: {matchedTests[0]}", fg="bright_green")
    ret = requests.get(str(dbURL))

    if ret.ok:
        if matchedTests[0] != "leapp.brew-build.upgrade.distro":
            dbResponse = ret.json()
            testcaseDict = {
                "Name": dbResponse["testcase"]["name"],
                "Run Details": dbResponse["ref_url"],
                "Status": dbResponse["outcome"],
                "Testsuite Owner": dbResponse["data"]["ci_team"][0],
                "Slack/IRC": dbResponse["data"]["ci_irc"][0],
                "UMB URL": umbURL,
            }
            return testData(testcaseDict, umbURL)

    return testData({}, umbURL)


# queries datagrepper instance for UMB JSON data
def umbSearch(umbURL):
    ret = requests.get(umbURL)

    if ret.ok:
        umbResponse = ret.json()
        try:
            xunitDecoded = xmltodict.parse(
                gzip.decompress(base64.b64decode(umbResponse["msg"]["test"]["xunit"]))
            )
        except:
            click.secho(
                "  Unable to parse xunit UMB Message from Datagrepper",
                fg="red",
                bold=True,
            )
            sys.exit()

        # Assumption here is that UMB message contains at most 1 "testsuite"
        # (which contains multiple testcases) based on JSON data.
        testsuiteNames = [xunitDecoded["testsuites"]["testsuite"]["@name"]]

        # list of lists of testcases in umb MSG
        # allows us to zip with testsuiteNames to create dict.
        testcases = []

        if isinstance((xunitDecoded["testsuites"]["testsuite"]["testcase"]), dict):
            testcases.append(
                [
                    xunitDecoded["testsuites"]["testsuite"]["testcase"]["@name"],
                    xunitDecoded["testsuites"]["testsuite"]["testcase"]["@result"],
                ]
            )
            return dict(zip(testsuiteNames, testcases))

        for item in xunitDecoded["testsuites"]["testsuite"]["testcase"]:
            testcases.append(item["@name"])
            testcases.append(item["@result"])
        testcases = [testcases]

    return dict(zip(testsuiteNames, testcases))


####################
# PRINT  FUNCTIONS #
####################

# These functions click.echo using the columnar library.
# Standard procedure is to creat temporary lists,
# and format the data into said temp lists.


def osciTable(data, condition):
    if data is None:
        sys.exit(
            click.style(
                """   No OSCI Data is available for this build.
                \n  Please try running the tool again. If the issue persists,
                \n  Please visit OSCI Dashboard Web UI or contact debarbos
                """,
                fg="red",
                bold=True,
            )
        )

    # Creating a list of lists to satisfy columnar typing.
    keys = [*data.keys()]
    values = [*data.values()]

    if len(keys) < 1:
        click.secho(f"  No {condition} Requirements Found", fg="bright_yellow")
        return

    osciColumnar = [[keys[i], values[i]] for i in range(len(keys))]
    headers = [f"{condition} Test", "Test Reference"]

    try:
        click.echo(columnar(osciColumnar, headers, no_borders=True))
    except:
        sys.exit(click.secho("  Unable to Format OSCI Data", fg="red", bold=True))


def brewTable(data):
    if data is None:
        sys.exit(
            click.style(
                """  No Brew Data is available for this build
            \n  Either you have entered an incorrect ID/NVR or there is no build data available
            \n  Please try running the tool again, if the issue persists, please visit the Brew web UI
            \n  Or contact debarbos
            """,
                fg="red",
                bold=True,
            )
        )

    # Creating a list of lists to satisfy columnar typing.
    keys = [*data.keys()]
    values = [*data.values()]

    brewColumnar = [[keys[i], values[i]] for i in range(len(values))]
    brewHeaders = ["Brew-Field", "Data"]
    patterns = [
        ("nvr", lambda text: click.style(text, fg="cyan", bold=True)),
        ("taskID", lambda text: click.style(text, fg="cyan", bold=True)),
    ]

    try:
        click.echo(
            columnar(brewColumnar, brewHeaders, patterns=patterns, no_borders=True)
        )
    except:
        sys.exit(click.secho("  Unable to Format Brew Data", fg="red", bold=True))


def waivedTable(waivedList):
    if len(waivedList) > 0:
        click.secho("\n  WAIVED TESTS\n")
        for item in waivedList:
            click.secho(f"  {item}")
    else:
        click.secho("\n  No Waived Tests", bold=True)


# Creates Columnar based on specified testcase's resultsDB data.
def testcaseTable(data):
    if data is not None:
        # Creating a list of lists to satisfy columnar typing.
        keys = [*data.keys()]
        values = [*data.values()]

        testcaseColumnar = [[keys[i], values[i]] for i in range(len(values))]
        headers = ["Testsuite-Field", "Metadata"]
        try:
            click.echo(columnar(testcaseColumnar, headers, no_borders=True))
            if data["Status"] == "FAILED" or data["Status"] == "NEEDS_INSPECTION":
                click.secho(
                    f"  TIP: Waive this testsuite with: git kmt-waive -t {data['Name']} -c $COMMENT -k $NVR",
                    bold=True,
                )
        except:
            sys.exit(
                click.secho("  Unable to Format Testcase Data", fg="red", bold=True)
            )


# Creates Columnar based on specified testcase's UMB data.
def umbTable(data):
    if data is not None:
        umbColumnar = []
        headers = ["Testcase", "Results"]

        for key in data:
            click.secho(f"\n\n  TESTSUITE: {key}")
            umbColumnar += [
                [data[key][i], data[key][i + 1]]
                for i in range(0, len(data[key]) - 1, 2)
            ]
            try:
                click.echo(columnar(umbColumnar, headers, no_borders=True))
            except:
                sys.exit(
                    click.secho("\n  Unable to format UMB Data", fg="red", bold=True)
                )


#########################
# CLICK COMMANDS & ARGS #
#########################


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option("--id", "-i", help="Search using Brew Task ID (aka) OSCI Task ID")
@click.option("--nvr", "-n", help="Search using a Kernel NVR")
@click.option(
    "--save",
    "-s",
    is_flag=True,
    default=False,
    help="Save Command Output to a text file in /tmp",
)
@click.option(
    "--show-testcase",
    "-t",
    "testcase",
    help="""Isolate a singular testsuite, and echo its ResultsDB data. Requires an NVR or BrewID field to be
              satisfied as a prerequisite""",
)
@click.version_option(version="v1.2.1", package_name="igrab", prog_name="igrab")
def main(id, nvr, save, testcase):
    """
    \b
    This script retrieves test results for kernel builds.
    Specifically, any Kernels built with the Brew Buildsystem
    can be tracked with this script, given that a proper NVR
    and a proper Brew TASK ID (not Build ID) is provided.
    """

    # initalize a cliData object to store the ID, NVR fields and testcase flag
    argVal = cliData(id, nvr, testcase)

    # check save flag prior to tool logic loop
    if save:
        time = datetime.now().strftime("%y-%m-%d-%H:%M:%S")
        click.secho(
            f"Writing File to: \n /usr/tmp/igrab-{time}.txt",
            fg="bright_yellow",
            bold=True,
        )
        try:
            fp = open(f"/usr/tmp/igrab-{time}.txt", "w")
            sys.stdout = fp
        except:
            sys.exit(
                click.secho(
                    "  Unable to open/write to file /usr/tmp/", fg="red", bold=True
                )
            )

    if argVal.nvr is not None:
        osciResponse = nvrHandler(argVal.nvr)
    elif argVal.id is not None:
        osciResponse = idHandler(argVal.id)
    else:
        sys.exit(click.style("Please supply an option \n", fg="bright_red", bold=True))

    if argVal.testcase:
        testResponse = testcaseHandler(argVal, osciResponse, testcase)
        testcaseTable(testResponse.testcaseData)
        umbTable(umbSearch(testResponse.umbURL))
    else:
        waivedTable(osciResponse.waived)
        osciTable(osciResponse.satisfied, "Satisfied")
        osciTable(osciResponse.unsatisfied, "Unsatisfied")
    if save:
        fp.close()


if __name__ == "__main__":
    main()
